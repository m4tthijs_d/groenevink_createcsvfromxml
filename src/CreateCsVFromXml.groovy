import javax.xml.transform.TransformerFactory
import javax.xml.transform.stream.StreamResult
import javax.xml.transform.stream.StreamSource

//in- and outputfolders
def inputDir = "/Users/matthijsdrenth/Workspaces/Oxygen/iWlz/2.0.2 v1.9/XML/"
def outputDir = "/Users/matthijsdrenth/Workspaces/Oxygen/iWlz/2.0.2 v1.9/CSV/"

// Load xslts
def xslt1=new File("../resources/removeNS.xsl").getText()
def xslt2= new File("../resources/extractAllXpaths.xsl").getText()

// Create transformer
def transformer1 = TransformerFactory.newInstance().newTransformer(new StreamSource(new StringReader(xslt1)))
def transformer2 = TransformerFactory.newInstance().newTransformer(new StreamSource(new StringReader(xslt2)))


new File(inputDir.toString()).eachFile() { file->
    filename = file.getName()
    if ((filename.toString() =~ /.[a-z]*/)[-1] == ".xml") {
        println("converting" + filename + "to .csv and .txt")

        // Load xml
        def xmlInLoc = inputDir+filename
        def xmlIn = new File(xmlInLoc).getText()

        // Set output file
        def xmlOutLoc = outputDir+filename+"NONS.txt"
        def outputXml = new FileOutputStream(xmlOutLoc)

        // Perform 1st transformation
        transformer1.transform(new StreamSource(new StringReader(xmlIn)), new StreamResult(outputXml))

        // Set output file
        def txtOutLoc = outputDir+filename+".txt"
        def txtIn = new File(xmlOutLoc).getText()
        def outputTxt = new FileOutputStream(txtOutLoc)

        // Perform 2nd transformation
        transformer2.transform(new StreamSource(new StringReader(txtIn)), new StreamResult(outputTxt))

        //do some renaming of values
        def txt = new File(txtOutLoc).getText()
        def csvOutLoc = outputDir+filename+".csv"

        def txt1 = txt.replaceAll("/Bericht/Header/", "")
        def txt2 = txt1.replaceAll("/Bericht/", "")
        def txt3 = txt2.replaceAll("Clienten/", "")

        def newString = 'TestCase;'

        txt3.eachLine { line, count ->
            newString = newString + line + ';' // Output: line 0: Groovy is closely related to Java,
        }
        File file2 = new File(csvOutLoc)
        file2.text = newString

        def newcsvname = csvOutLoc.replaceAll(".xml", "")
        file2.renameTo newcsvname

        boolean fileSuccessfullyDeleted =  new File(xmlOutLoc).delete()

    }
}
